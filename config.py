import logging
from redis import StrictRedis

class Config(object):
    #项目的配置
    # DEBUG=True
    SECRET_KEY="HEIMA"
#加载数据库配置
    SQLALCHEMY_DATABASE_URI="mysql://root:12345678@127.0.0.1:3306/project01"
    #是否被修改
    SQLALCHEMY_TRACK_MODIFICATIONS=False

    # 在请求结束时候，如果指定此配置为 True ，那么 SQLAlchemy 会自动执行一次 db.session.commit()操作
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    #redis 的配置
    REDIS_HOST="127.0.0.1"
    REDIS_PORT=6379

    #session 保存配置
    SESSION_TYPE="redis"
    #开启session签名
    SESSION_USE_SIGNER=True
    #指定session 保存的redis
    SESSION_REDIS=StrictRedis(host=REDIS_HOST,port=REDIS_PORT)
    #设置需要过期
    SESSION_PERMANENT=False
    #设置过期时间
    PERMANENT_SESSION_LIFETIME=86400*2

    #设置日志等级
    LOG_LEVEL=logging.DEBUG

class DevelopmentConfig(Config):
    #开发环境下的配置
    # 项目的配置
    DEBUG = True


class ProductionConfig(Config):
    #生产环境配置
    DEBUG = False
    #设置日志等级
    LOG_LEVEL = logging.WARNING


class TestConfig(Config):
    #单元测试环境
    DEBUG = True
    TESTING=True
#通过不同的key获取不同的值，
Config={
    "development":DevelopmentConfig,
    "production":ProductionConfig,
    "testing":TestConfig
}





