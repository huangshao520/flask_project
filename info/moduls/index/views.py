# 所有的视图函数都放在当前文件夹下
from flask import render_template, current_app, g, session, request, jsonify
from sqlalchemy.sql.functions import user

from info import redis_store, constants
from info.models import News, User, Category
from info.utils.common import user_login_data
from info.utils.response_code import RET
from . import index_blu


@index_blu.route("/")
@user_login_data
def index():
    # session["name"]="itheima"
    # logging.debug("测试debug")render_template("news/index.html")
    # logging.warning("警告debug")
    # logging.error("错误debug")
    # logging.fatal("测试debug")
    #
    # #flask里面日志输出
    # current_app.logger.error("测试错误")

    # 向redis 中保存一个值 name itcase
    # redis_store.set("name","itcadt")
    #查询用户信息
    # user_id = session.get("user_id", None)
    # user = None
    # if user_id:
    #     # 尝试查询用户的模型
    #     try:
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.error(e)

    user = g.user
    # 右侧的新闻排行的逻辑

    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    news_list_li = []
    for news in news_list:
        news_list_li.append(news.to_basic_dict())

    data = {
        "user": user.to_dict() if user else None,
        "news_dict_li": news_list_li
    }

    return render_template('news/index.html', data=data)

    # return render_template('news/index.html')


# 当打开网页的时候浏览器会请求根路径+favicon做为网站标签的小图标
@index_blu.route('/favicon.ico')
def favicon():
    # 怎么找小图标路径？current_app=self:当前文件(注意点：首先清除浏览器的数据，然后在强制退出浏览器）
    return current_app.send_static_file("news/favicon.ico")


# send_static_file是flask去查找指定的静态文件的所调用的方法


@index_blu.route("/news_list")
def news_list():
    "获取首页新闻"
    # 1 获取参数
    # 新闻的分类Id
    print(request.args)
    # 新闻6个分类
    cid = request.args.get("cid", "1")
    # x新闻页数
    page = request.args.get("page", "1")
    # 每页新闻条数
    per_page = request.args.get("per_page", "10")

    # 2 检验参数，是不是整数
    try:
        page = int(page)
        cid = int(cid)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errnsg="参数不是整形")
    filters = []
    if cid != 1:  # 查询的不是最新的数据
        # 需要添加条件
        filters.append(News.category_id == cid)

    # 3 查询数据(查询filters列表中，根据创建时间先从大到小每页新闻)
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page, per_page,
                                                                                          False)
    except Exception as e:
        current_app.logger.error(e)

        return jsonify(errno=RET.DBERR, errmsg="数据查询错误")

    # 取到当前页的数据
    news_model_list = paginate.items  # 模型对象列表
    total_page = paginate.pages
    current_page = paginate.page

    # 将模型对象列表转成字典列表
    news_dict_li = []

    for news in news_model_list:
        news_dict_li.append(news.to_basic_dict())  # 把news转成字典

    #分类查询数据，通过模板的形式渲染出来
    categories = Category.query.all()
    category_li = []
    for category in categories:
        category_li.append(category.to_dict())
    # print(category_li)


    data = {
        "total_page": total_page,
        "current_page": current_page,
        "news_dict_li": news_dict_li,
        "category_li":category_li
    }


    return jsonify(errno=RET.OK, errmsg="OK", data=data)
