from flask import Flask, sessions, session, current_app
from flask_sqlalchemy import SQLAlchemy
#可以用来指定session保存的位置
from flask_session import  Session
from flask.ext.wtf import CSRFProtect
from redis import StrictRedis
from flask_script import Manager
from flask_migrate import Migrate,MigrateCommand
#MigrateCommand(迁移的命令)
from config import Config
from info import create_app ,db,models
import  logging

#namage.py是程序启动的入口，只关心启动时相关的参数以及内容，不关心具体
#如果创建app或者相关业务逻辑
#通过指定的配置名字创建对应的app
from info.models import User

app=create_app("development")#crea_app类似于工厂方法

manager=Manager(app)

#将APP于db关联
Migrate(app , db )
#将迁移命令添加到，manager中
manager.add_command("db",MigrateCommand)

@manager.option('-n', '-name', dest="name")
@manager.option('-p', '-password', dest="password")
def createsuperuser(name, password):

    if not all([name, password]):
        print("参数不足")

    user = User()
    user.nick_name = name
    user.mobile = name
    user.password = password
    user.is_admin = True

    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)

    print("添加成功")



if __name__ == '__main__':
    # print("1", app.url_map)
    manager.run()